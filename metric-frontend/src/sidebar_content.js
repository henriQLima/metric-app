import React from 'react';
import MaterialTitlePanel from './material_title_panel';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Save from '@material-ui/icons/Save';
import Map from '@material-ui/icons/Map'
import Refresh from '@material-ui/icons/Refresh'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText'
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';


const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#62727b',
      main: '#37474f',
      dark: '#102027',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#000',
    },
  },
});

const styles = require('./styles/sidebarstyles.js')

const SidebarContent = (props) => {
  const style = props.style ? { ...styles.sidebar, ...props.style } : styles.sidebar;

  const links = [];
  console.log(props.projects)

  //projects from server
  for (let ind = 0; ind < props.projects.length; ind++) {
    links.push(
      <div style={styles.sidebarLink}>
        <Link style={{ textDecoration: 'none' }} to={'/metric/' + props.projects[ind]._id + '/2Dmap'}>
          <Button style={styles.btntxt} onClick={() => { props.selectProject(props.projects[ind]._id, props.projects[ind].location) }} variant="raised" color={(props.projects[ind]._id === props.selectedProject) ? 'secondary' : 'primary'} size="large" aria-label="add">
            <Map />
            {props.projects[ind].name}
          </Button>
        </Link>
      </div>)
  }

  return (
    <MuiThemeProvider theme={theme}>
      <MaterialTitlePanel style={styles.bg}>
        <div style={styles.content}>

          <div style={styles.sidebarLink}>
            <Chip
              avatar={<Avatar>HL</Avatar>}
              label="HENRIQUE LIMA"
            />
          </div>

          <div style={styles.divider} />
          <a href="index.html" style={styles.sidebarLink}>{props.selectedProject}</a>

          <div style={styles.divider} />

          <div style={styles.addbtns}>

            <Button style={{ float: 'left' }} onClick={props.addProject} variant="raised" size="small" aria-label="add">
              <Save />
              add new
          </Button>

            <Button style={{ float: 'right' }} onClick={props.refreshProject} size="small" variant="raised" aria-label="load">
              <Refresh />
            </Button>

          </div>
          {links}
        </div>
      </MaterialTitlePanel>
    </MuiThemeProvider>
  );
};

SidebarContent.propTypes = {
  style: PropTypes.object,
};


export default SidebarContent;