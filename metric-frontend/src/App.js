import React, { Component } from 'react';
import './App.css';
import MetricMap from './MetricMap';
import MetricSidebar from './MetricSidebar';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import axios from 'axios';
import Metric3D from './Metric3D';
import { Route } from 'react-router-dom';
import MetricSidebarProjects from './MetricSidebarProjects';
import GeoJsonLoader from './GeoJsonLoader'
import Login from './login'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#62727b',
      main: '#37474f',
      dark: '#102027',
      contrastText: '#fff',
    },
    secondary: {
      light: '#484848',
      main: '#212121',
      dark: '#000000',
      contrastText: '#000',
    },
  },
});

class App extends Component {
  constructor() {
    super()
    this.state = {
      selectedProject: 'SELECT A PROJECT',
      selectStatus: false,
      projects: [],
      features: [],
      location: [-46.586436, -21.811709],
      ortoSelector: 'ORTOMOSAIC7',
      cadSelector: false
    }
  }

  componentDidMount() {
    axios.get('/metric/projects')
      .then(result => {
        let newDatafromServer = result.data;
        this.setState({ projects: newDatafromServer })
      })
      .catch(error => {
        console.log(error)
      })
  }

  //adding a new tast to state in 4 steps
  addProject = () => {

    let name = ('project ' + (Math.random() * 1000).toFixed(0))
    //1. create a new object
    let newProject = {
      name: name
    };

    axios.post('/metric/projects', newProject)
      .then((result) => {
        console.log('new project added:', result);
        //2. copy the original state
        let copy = Array.from(this.state.projects);
        //3. push the new task into copy
        copy.push(result.data);
        //4. return the copy to replace the original state
        this.setState({
          projects: copy
        });
      })
      .catch(error => {
        console.log(error)
      })
  }

  selectProject = (projectId, location) => {
    this.setState({ selectedProject: projectId })
    this.setState({ location: location })
    this.setState({ selectStatus: true })

    console.log('location from DB', location)

    axios.get('/metric/features/' + projectId)
      .then(result => {
        console.log('features from server:', result.data)
        //
        let newSelectedProj = result.data;
        //
        this.setState({
          features: newSelectedProj
        });
      })
      .catch(error => {
        console.log(error)
      })
  }

  ortoSelectorFunction = event => {
    this.setState({ ortoSelector: event.target.value });
  }

  cadSelectorFunction = event => {
    this.setState({ cadSelector: event.target.checked });
    console.log(this.state.cadSelector)
  }

  refreshProject = () => {
    axios.get('/metric/features/' + this.state.selectedProject)
      .then(result => {
        console.log('features from server:', result.data)
        //
        let newSelectedProj = result.data;
        //
        this.setState({
          features: newSelectedProj
        });
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    return (
      <div>
        <MuiThemeProvider theme={theme}>

          <Route path='/metric/:projectId' render={(props) => <MetricSidebarProjects
            projectId={props.match}
            projects={this.state.projects}
            addProject={this.addProject}
            selectProject={this.selectProject}
            selectedProject={this.state.selectedProject}
            ortoSelectorFunction={this.ortoSelectorFunction}
            ortoSelector={this.state.ortoSelector}
            cadSelectorFunction={this.cadSelectorFunction}
            cadSelector={this.state.cadSelector}
            />} />

          <Route exact path="/metric" render={() => <MetricSidebar
            projects={this.state.projects}
            addProject={this.addProject}
            selectProject={this.selectProject}
            selectedProject={this.state.selectedProject}
          />} />

          <Route exact path="/metric/:projectId/3dmap" render={() => <Metric3D />
          } />

          <Route exact path="/login" render={() => <Login />
          } />
          
          <Route path="/metric" render={() => <MetricMap selectProject={this.selectProject}
            selectedProject={this.state.selectedProject}
            saveProj={this.saveProj}
            features={this.state.features}
            location={this.state.location}
            selectStatus={this.state.selectStatus} 
            ortoSelector={this.state.ortoSelector}
            cadSelector={this.state.cadSelector}/>
          } />

          {/* <Route path="/metric" render={() => <GeoJsonLoader selectProject={this.selectProject}
            selectedProject={this.state.selectedProject}
            saveProj={this.saveProj}
            features={this.state.features}
            location={this.state.location}
            selectStatus={this.state.selectStatus}
            ortoSelector={this.state.ortoSelector} />
          } /> */}

        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
