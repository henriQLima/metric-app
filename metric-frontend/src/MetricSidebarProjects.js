import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

import Sidebar from 'react-sidebar';
import MaterialTitlePanel from './material_title_panel';
import SidebarContent from './sidebar_content';
import ProjectContent from './project_content';

import Button from '@material-ui/core/Button';
import Menu from '@material-ui/icons/Menu';

const styles = {
    contentHeaderMenuLink: {
        textDecoration: 'none',
        color: 'white',
        padding: 8,
    },
    content: {
        padding: '16px',
    },
    sidebarbtn: {
        fontSize: '1px',
        minWidth: '1px',
        minHeight: '1px',
        color: 'white',
        position: 'absolute',
        top: '27%',
        left: '0px',
        display: 'flex',
        opacity: .9,
        zIndex: 3
    }
};

class MetricSidebarProjects extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            docked: true,
            open: false,
            transitions: true,
            touch: true,
            shadow: true,
            pullRight: false,
            touchHandleWidth: 20,
            dragToggleDistance: 30,
            valueCheck: "henriqlima.76jdwuqh"
        };

    }

    onSetOpen = (docked) => {
        this.setState({ docked: docked });
    }

    menuButtonClick = (ev) => {
        ev.preventDefault();
        this.onSetOpen(!this.state.docked);
    }

    renderPropNumber = (prop) => {
        const setMethod = (ev) => {
            const newState = {};
            newState[prop] = parseInt(ev.target.value, 10);
            this.setState(newState);
        };

        return (
            <p key={prop}>
                {prop} <input type="number" onChange={setMethod} value={this.state[prop]} />
            </p>);
    }

    render() {

        const sidebarContProps = {
            projects: this.props.projects,
            addProject: this.props.addProject,
            selectProject: this.props.selectProject,
            selectedProject: this.props.selectedProject,
            ortoSelectorFunction: this.props.ortoSelectorFunction,
            valueCheck: this.state.valueCheck,
            ortoSelector: this.props.ortoSelector,
            cadSelectorFunction: this.props.cadSelectorFunction,
            cadSelector: this.props.cadSelector
        };

        const sidebar = <ProjectContent {...sidebarContProps}/>

        /*props from the sidebar in this variable getting data from state*/
        const sidebarProps = {
            sidebar: sidebar,
            docked: this.state.docked,
            sidebarClassName: 'custom-sidebar-class',
            open: this.state.open,
            touch: this.state.touch,
            shadow: this.state.shadow,
            pullRight: this.state.pullRight,
            touchHandleWidth: this.state.touchHandleWidth,
            dragToggleDistance: this.state.dragToggleDistance,
            transitions: this.state.transitions,
            onSetOpen: this.onSetOpen,
            zIndex: 3
        };

        return (
            <Sidebar {...sidebarProps /*props from this sidebar in a variable above*/}>
                <Button style={styles.sidebarbtn} variant="raised" color='secondary' onClick={this.menuButtonClick}>
                    <Menu />
                </Button>
            </Sidebar>
        );
    }
}

export default MetricSidebarProjects;