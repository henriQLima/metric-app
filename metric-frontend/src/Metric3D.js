import React, { Component } from 'react';
import PropTypes from 'prop-types';


class Metric3D extends Component {
    componentDidMount() {
        var iframe = document.getElementById('api-frame');
        var version = '1.0.0';
        var urlid = 'd592b521bd20404a8fb4192bbe679fbf';
        var client = new window.Sketchfab(version, iframe);

        client.init(urlid, {
            success: (api) => {
                api.start();
                api.addEventListener('viewerready', () => {

                    // API is ready to use
                    // Insert your code here
                    console.log('Viewer is ready');
                });
            },
            error: () => {
                console.log('Viewer error');
            }
        });
    }

    render() {
        return (
                <iframe id="api-frame" 
                style={{overflow:'hidden',position:'absolute',float:'left', height:'100%', width:'100%', zIndex:2}} 
                allowfullscreen 
                mozallowfullscreen="true" 
                webkitallowfullscreen="true"
                frameborder="0"
                width="100%"
                height='100%'
                marginheight="0"
                marginwidth="0"
                scrolling="no"
                top='0'
                left='0'>
                </iframe>
        )
    }
}

export default Metric3D;