import React from 'react';
import MaterialTitlePanel from './material_title_panel';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Save from '@material-ui/icons/Save';
import Map from '@material-ui/icons/Map'
import ArrowBack from '@material-ui/icons/ArrowBack'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText'
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import Terrain from '@material-ui/icons/Terrain';
import Layers from '@material-ui/icons/Layers';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';


const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#62727b',
      main: '#37474f',
      dark: '#102027',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#000',
    },
  },
});

const styles = require('./styles/sidebarstyles.js')

const ProjectContent = (props) => {
  // const style = props.style ? { ...styles.sidebar, ...props.style } : styles.sidebar;

  // const links = [];
  // console.log(props.projects)

  //projects from server
  // for (let ind = 0; ind < props.projects.length; ind++) {
  //   links.push(
  //     <div style={styles.sidebarLink}>
  //       <Button style={styles.btntxt} onClick={() => { props.selectProject(props.projects[ind]._id, props.projects[ind].location) }} variant="raised" color={(props.projects[ind]._id === props.selectedProject) ? 'secondary' : 'primary'} size="large" aria-label="add">
  //         <Map />
  //         {props.projects[ind]._id}
  //       </Button>
  //     </div>)
  // }

  return (
    <MuiThemeProvider theme={theme}>
      <MaterialTitlePanel style={styles.bg}>
        <div style={styles.content}>

          <div style={styles.sidebarLink}>
            <Chip
              avatar={<Avatar>HL</Avatar>}
              label="HENRIQUE LIMA"
            />
          </div>

          <div style={styles.divider} />
          <a href="index.html" style={styles.sidebarLink}>{props.selectedProject}</a>

          <div style={styles.divider} />

          <div style={styles.addbtns}>

            <Link to='/metric'>
              <Button style={{ float: 'left' }} size="small" variant="raised" aria-label="load">
                <ArrowBack />
              </Button>
            </Link>
          </div>

          <div style={styles.divider} />

          <div style={{ padding: '15px', display: 'inline-flex' }}>
            <BottomNavigation>
              <Link style={{ textDecoration: 'none' }} to={'/metric/' + props.selectedProject + '/2DMap'}>
                <BottomNavigationAction showLabel='true' label="2D MAP" icon={<Layers />} />
              </Link>

              <Link style={{ textDecoration: 'none' }} to={'/metric/' + props.selectedProject + '/3DMap'}>
                <BottomNavigationAction showLabel='true' label="3D MAP" icon={<Terrain />} />
              </Link>
            </BottomNavigation>
          </div>

          <div style={styles.sidebarLink}>
            <ExpansionPanel>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography style={{ color: 'gray' }}>2D MAP SELECTOR</Typography>
              </ExpansionPanelSummary>

              <FormControl component="fieldset" required>
                <RadioGroup
                  aria-label="ortoSelector"
                  name="ortoSelector"
                  value={props.ortoSelector}
                  onChange={props.ortoSelectorFunction}
                >
                  <FormGroup>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={props.cadSelector}
                          onChange={props.cadSelectorFunction}
                          value="AUTOCAD LAYER"
                        />
                      }
                      label="AUTOCAD LAYER"
                    />
                  </FormGroup>

                  <FormControlLabel value="JFLDTM" control={<Radio />} label="Digital Terrain Model" />
                  <FormControlLabel value="ORTOMOSAIC1" control={<Radio />} label="March 08, 2018" />
                  <FormControlLabel value="ORTOMOSAIC2" control={<Radio />} label="March 30, 2018" />
                  <FormControlLabel value="ORTOMOSAIC3" control={<Radio />} label="April 17, 2018" />
                  <FormControlLabel value="ORTOMOSAIC4" control={<Radio />} label="April 29, 2018" />
                  <FormControlLabel value="ORTOMOSAIC5" control={<Radio />} label="May 11, 2018" />
                  <FormControlLabel value="ORTOMOSAIC6" control={<Radio />} label="June 01, 2018" />
                  <FormControlLabel value="ORTOMOSAIC7" control={<Radio />} label="June 13, 2018" />

                </RadioGroup>
              </FormControl>

            </ExpansionPanel>

          </div>
        </div>
      </MaterialTitlePanel>
    </MuiThemeProvider>
  );
};

ProjectContent.propTypes = {
  style: PropTypes.object,
};


export default ProjectContent;