const bgimage = '/img/sidebar-1.jpg'
const styles = {
  sidebar: {
    fontFamily: "Roboto",
    textAlign: 'center',
    width: 280,
    height: '100%',
    transition: "all 300ms linear",
    display: "block",
    opacity: 20,
    transform: "translate3d(0px, 0, 0)",
    fontSize: "20px",
    whiteSpace: "nowrap",
    overflow: "hidden",
    color: "white",
    zIndex: 3
  },
  sidebarLink: {
    display: 'block',
    padding: '10px 15px',
    color: 'white',
    textDecoration: 'none',
    fontSize: '14px',
    fontFamily: "Roboto , Helvetica, Arial, sans-serif",
    fontWeight: 300,
    lineHeight: '30px',
    capitalize: true
  },
  divider: {
    paddingRight: '10px',
    margin: '1px 0',
    height: 1,
    position: "absolute",
    right: "15px",
    height: "1px",
    width: "calc(100% - 40px)",
    backgroundColor: "hsla(0,0%,100%,.3)"
  },
  content: {
    textAlign: 'center',
    paddingTop: '5px',
    height: '100%',
    color: "inherit",
    height: "auto",
    margin: 0,
    display: "block",
    position: "relative",
    transform: "translate3d(0px, 0, 0)",
    fontSize: "13px",
    transition: "transform 300ms ease 0s, opacity 300ms ease 0s",
    fontFamily: "Roboto , Helvetica, Arial, sans-serif",
    lineHeight: "30px",
    fontWeight: 300,
    whiteSpace: "nowrap",
    zIndex: 3
  },
  bg: {
    width: 295,
    height: '100%',
    zIndex: 3,
    content: "",
    display: 'block',
    position: 'relative',
    backgroundImage: `url(${bgimage})`,
    backgroundSize: 'cover',
    opacity: .98
  },
  addbtns: {
    display: 'block',
    padding: '15px 20px 50px 20px',
  },
  btntxt: {
    textDecoration: 'none',
    fontSize: '14px',
    fontFamily: "Roboto , Helvetica, Arial, sans-serif",
    fontWeight: 300,
    lineHeight: '30px'
  }
};

//export variables as external file
module.exports = styles;