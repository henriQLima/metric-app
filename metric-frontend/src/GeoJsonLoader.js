import React, { Component } from 'react';
import ReactMapboxGl, { Layer, Feature, Popup, Source, ScaleControl, ZoomControl, RotationControl, GeoJSONLayer } from 'react-mapbox-gl';
import DrawControl from 'react-mapbox-gl-draw';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
import styled from 'styled-components'
import * as turf from '@turf/turf'
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Save from '@material-ui/icons/Save';
import Menu from '@material-ui/icons/Menu';
import Warning from '@material-ui/icons/Warning';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Arrow } from 'react-popper';
import MetricSidebar from './MetricSidebar';
import axios from 'axios';
import mapboxgl from 'mapbox-gl'

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#62727b',
            main: '#37474f',
            dark: '#102027',
            contrastText: '#fff',
        },
        secondary: {
            light: '#484848',
            main: '#212121',
            dark: '#000000',
            contrastText: '#000',
        },
    },
});

const styles = {
    sidebarbtn: {
        fontSize: '1px',
        minWidth: '1px',
        minHeight: '1px',
        color: 'white',
        position: 'absolute',
        top: '27%',
        left: '0px',
        display: 'flex',
        opacity: .9
    }
}

const Map = ReactMapboxGl({
    accessToken: 'pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w',
    maxZoom: 23
});

// const Container = styled.div`
//   position: relative;
//   height: 100%;
//   flex: 1;
// `;

const Logo = styled.div`
position:absolute;
background-size: 230px;
height: 100px;
width: 190px;
right:55px;
top:-25px;
background-image: url("/img/metric.png");
z-index: 2
`;

const BottomBar = styled.div`
  position: absolute;
  bottom: 20px;
  right: 20px;
  height: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

// const menuButton = styled.button`
// border: 1px solid #3770c6;
// background-color: rgb(84, 152, 255);
// height: 100%;
// color: white;
// font-size: 13px;
// padding: 6px 12px;
// border-radius: 6px;
// cursor: pointer;
// outline: none;
// :hover {
//   background-color: #3770c6;
// }
// `;

const StyledPopup = styled.div`
  background: white;
  color: #3f618c;
  font-weight: 400;
  padding: 5px;
  border-radius: 2px;
`;

const ShowArea = styled.div`
  position: absolute;
  text-align: center;
  top: 0px;
  left: 297px;
  height: 18px;
  width: 150px;
  color: #1a1a1a;
  background-color: white;
  font-weight: 400;
  font-size: 13px;
  padding: 5px;
  border-radius: 2px;
  opacity: .9;
`;

const ShowCoords = styled.div`
  position: absolute;
  text-align: center;
  bottom: 0px;
  left: 50%;
  height: 14px;
  width: 290px;
  color: #1a1a1a;
  background-color: white;
  font-weight: 400;
  font-size: 11px;
  padding: 5px;
  border-radius: 2px;
  opacity: .9;
`;

/////MAPBOX CONFIGS//////
const flyToOptions = {
    speed: 0.6
};

const paintFeatures = {
    "line-width": 3,
    "fill-color": '["get", "color"]',
    "fill-opacity": 0.5
}

// COMPONENT THAT CONTAINS THE MAP AND ALL THE FUNCTIONS
class GeoJsonLoader extends Component {

    constructor() {
        super();
        this.state = {
            mapCoords: 'lat: 0, lng: 0',
            selectedProject: undefined,
            features: [],
            fitBounds: undefined,
            center: [-21.811709, -46.586436],
            zoom: [16],
            station: undefined,
            stations: {},
            measure: {
                unit: '',
                type: 'select feature',
                value: 0
            }
        }
    }

    componentDidUpdate() {
        let featuresFromApp = this.props.features

        featuresFromApp.forEach(element => {
            var pageFeature = {
                id: element.id,
                type: element.type,
                properties: { color: '#e02c95' },
                geometry: {
                    type: element.geometry.type,
                    coordinates: element.geometry.coordinates,
                },
                paint: {
                    "line-width": 3,
                    "fill-color": ["get", "color"],
                    "fill-opacity": 0.5
                }
            };
            var addedIds = this.drawControl.draw.add(pageFeature)
            //console.log(addedIds)
        });

        // console.log(typeof(this.props.selectedProject))

        //console.log('Project Features:', this.props.features)
    }
    ////

    saveProj = () => {
        let draw = this.drawControl.draw.getAll()
        var featuresArr = []
        var serverArr = []

        //0. CAPTURING ALL FEATURES
        //if (draw.features.length > 0) {
        for (let i = 0; i < draw.features.length; i++) {
            let newFeature = {
                id: draw.features[i].id,
                project: this.props.selectedProject,
                type: draw.features[i].type,
                geometry: {
                    type: draw.features[i].geometry.type,
                    coordinates: draw.features[i].geometry.coordinates
                },
            }
            serverArr.push(newFeature)
        }

        console.log('1. test', serverArr)

        //1. CHECK IF THE FEATURE IS IN STATE
        if (this.state.features.length > 0) {
            for (var w = serverArr.length - 1; w >= 0; w--) {
                for (var j = 0; j < this.state.features.length; j++) {
                    if (this.state.features[j].id === serverArr[w].id) {
                        serverArr.splice(w, 1);
                        break;
                    }
                }
            }
        }

        console.log('2. newServerArr', serverArr)

        //2. SEND NEW FEATURES ONLY TO THE DB
        if (serverArr.length > 0) {
            axios.post('/metric/features', serverArr)
                .then(result => {
                    console.log(result)
                    //3. PUT NEW FEATURES IN STATE
                    console.log('3. state=>', this.state.features)
                    let copy = Array.from(this.state.features);
                    copy.push(...serverArr)
                    this.setState({
                        features: copy
                    })
                })
                .catch(error => {
                    console.log(error)
                })
        } else {
            console.log('all elements are the same')
        }
    }

    //MAP MOUSE HOVER
    mapHover = (map, evt) => {
        //Actual Coordinates
        //console.log('latlng', evt.lngLat);

        let lng = String((evt.lngLat.lng).toFixed(12))
        let lat = String((evt.lngLat.lat).toFixed(12))
        let myCoords = 'WGS84' + ' | ' + 'lat: ' + lat + ' - lng: ' + lng

        this.setState({ mapCoords: myCoords }) //<--ta dando pau!!

        // console.log('layerlotes=>',map.getLayer('layerLotes'));        

        // map.on('click', 'layerLotes', function (e) {
        //     console.log('click');
        //     new mapboxgl.Popup()
        //       .setLngLat(e.lngLat)
        //       .setHTML(e.features[0].properties.name)
        //       .addTo(map);
        // });
    }

    //MAP FEATURES FUNCTION ACTIVATED ON MAPCLICK
    mapClick = (map, evt) => {

        console.log(this.props.ortoSelector)
        //Actual Coordinates
        //console.log('latlng', evt.lngLat);

        console.log('i need that =>', map) //<= IMPORTANT

        console.log('draw controller =>', this.drawControl.draw.options)

        let selectedFeature = this.drawControl.draw.getSelected()

        console.log(selectedFeature)

        let layerName = 'layerLotes'

        map.addLayer({
            "id": 'layerLotes',
            "type": "line",
            "source": {
                "type": "geojson",
                "data": '/img/Lotes-JFL.geojson'

            },
            "layout": {
                "line-join": "round",
                "line-cap": "round",
                "visibility": "visible"
            },
            "paint": {
                "line-color": "red",
                "line-width": 2
            },
        });

        // var myLayer = L.mapbox.featureLayer()
        //     .loadURL('/img/Lotes-JFL.geojson')
        //     .on('ready', function () {
        //         myLayer.eachLayer(function (layer) {
        //             layer.bindPopup(layer.features.properties.name);
        //         });
        //     })
        //     .addTo(map);


        if (selectedFeature.features[0]) {
            //Calculating area and setting state
            if (selectedFeature.features[0].geometry.coordinates.length === 1) {
                console.log('é area');
                let polygon = turf.polygon([selectedFeature.features[0].geometry.coordinates[0]])
                var area = turf.area(polygon);
                return this.setState({
                    measure: {
                        unit: 'sq. mts',
                        type: 'area',
                        value: area
                    }
                })
            }
            else if (selectedFeature.features[0].geometry.coordinates.length === 2 && typeof selectedFeature.features[0].geometry.coordinates[0] === 'number') {

                let lat = selectedFeature.features[0].geometry.coordinates[0].toFixed(3)
                let lng = selectedFeature.features[0].geometry.coordinates[1].toFixed(3)

                let coord = lat + ',' + lng

                return this.setState({
                    measure: {
                        unit: 'mts',
                        type: 'coords',
                        value: coord
                    }
                })

            }
            else if (selectedFeature.features[0].geometry.coordinates.length >= 2) {
                console.log('é distancia');
                var line = turf.lineString(selectedFeature.features[0].geometry.coordinates);
                var length = turf.length(line, { units: 'kilometers' });
                return this.setState({
                    measure: {
                        unit: 'mts',
                        type: 'dist',
                        value: length * 1000
                    }
                })
            }
        } else {
            this.setState({
                measure: {
                    unit: '',
                    type: 'no feature',
                    value: 0
                }
            })
        }
    }

    /////////////////////////////////////////

    render() {
        return (
            <div className="App">
                <MuiThemeProvider theme={theme}>
                    <Map
                        style="mapbox://styles/mapbox/satellite-v9"
                        containerStyle={{
                            height: '100vh',
                            width: '100vw'
                        }}
                        center={this.props.location}
                        zoom={this.state.zoom}
                        flyToOptions={flyToOptions}
                        onClick={this.mapClick}
                        onMouseMove={this.mapHover}
                        onDblClick={this.mapRender}>

                        {/* <GeoJSONLayer
                            id='lotesLayer'
                            data='/img/Lotes-JFL.geojson'
                            lineLayout={{
                                'visibility': 'visible',
                                "line-join": "round",
                                "line-cap": "round"
                            }}
                            linePaint={{
                                'line-width': 1,
                                "line-color": '#f85467'
                            }}
                            sourceOptions={{
                                'type': 'geojson',
                                // 'filter': [["250.8", ["get", "Area"]]]
                            }}
                            symbolLayout={{
                                "text-field": "{place}",
                                "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
                                "text-offset": [0, 0.6],
                                "text-anchor": "top"
                            }}
                        /> */}

                        <Feature coordinates={[-46.58719577914829, -21.811482078342763]} />


                        <DrawControl
                            id='drawer'
                            ref={(drawControl) => { this.drawControl = drawControl; }}
                        />

                        <ZoomControl />
                        <ScaleControl position="bottom-left" />
                        <RotationControl />
                    </Map>

                    {(this.state.measure.type === 'area' || this.state.measure.type === 'dist') ?
                        <ShowArea>{this.state.measure.type}: {(this.state.measure.value).toFixed(2)} {this.state.measure.unit}</ShowArea>
                        :
                        <ShowArea>{this.state.measure.type}: {(this.state.measure.value)}</ShowArea>
                    }

                    <ShowCoords>{this.state.mapCoords}</ShowCoords>
                </MuiThemeProvider>
            </div >
        );
    }
}

export default GeoJsonLoader;