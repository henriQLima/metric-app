import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles/login.css'
import { Link } from 'react-router-dom';

class Login extends Component {
    render() {
        return (
            <div class="login-page">
                <div style={{textAlign: 'center'}}>
                    <img class='imgloader' src='/img/metric.1.png' />
                </div>
                <div class="form">
                    <form class="register-form">
                        <input type="text" placeholder="name" />
                        <input type="password" placeholder="password" />
                        <input type="text" placeholder="email address" />
                        <button>create</button>
                        <p class="message">Already registered? <a href="#">Sign In</a></p>
                    </form>
                    <form class="login-form">
                        <input type="text" placeholder="username" />
                        <input type="password" placeholder="password" />
                        <Link to='/metric'><button>login</button></Link>
                        <p class="message">Not registered? <a href="#">Create an account</a></p>
                    </form>
                </div>
            </div>
        );
    }
}

Login.propTypes = {

};

export default Login;