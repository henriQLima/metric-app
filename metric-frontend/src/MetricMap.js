import React, { Component } from 'react';
import ReactMapboxGl, { Layer, Feature, Popup, Source, ScaleControl, ZoomControl, RotationControl, GeoJSONLayer } from 'react-mapbox-gl';
import DrawControl from 'react-mapbox-gl-draw';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
import styled from 'styled-components'
import * as turf from '@turf/turf'
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Save from '@material-ui/icons/Save';
import Menu from '@material-ui/icons/Menu';
import Warning from '@material-ui/icons/Warning';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Arrow } from 'react-popper';
import MetricSidebar from './MetricSidebar';
import axios from 'axios';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#62727b',
            main: '#37474f',
            dark: '#102027',
            contrastText: '#fff',
        },
        secondary: {
            light: '#484848',
            main: '#212121',
            dark: '#000000',
            contrastText: '#000',
        },
    },
});

const styles = {
    sidebarbtn: {
        fontSize: '1px',
        minWidth: '1px',
        minHeight: '1px',
        color: 'white',
        position: 'absolute',
        top: '27%',
        left: '0px',
        display: 'flex',
        opacity: .9
    }
}

const Map = ReactMapboxGl({
    accessToken: 'pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w',
    maxZoom: 23,
    attributionControl: false
});

// const Container = styled.div`
//   position: relative;
//   height: 100%;
//   flex: 1;
// `;

const Logo = styled.div`
position:absolute;
background-size: 230px;
height: 100px;
width: 190px;
right:55px;
top:-25px;
background-image: url("/img/metric.png");
z-index: 2
`;

const Input = styled.input`
  position: absolute;
  top: 5px;
  left: 40px;
  display: flex;
  padding: 0.5em;
  margin: 0.5em;
  color: #102027;
  background: white;
  border: none;
  border-radius: 3px;
  opacity: 0.8
`;


const BottomBar = styled.div`
  position: absolute;
  bottom: 20px;
  right: 20px;
  height: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

// const menuButton = styled.button`
// border: 1px solid #3770c6;
// background-color: rgb(84, 152, 255);
// height: 100%;
// color: white;
// font-size: 13px;
// padding: 6px 12px;
// border-radius: 6px;
// cursor: pointer;
// outline: none;
// :hover {
//   background-color: #3770c6;
// }
// `;

const StyledPopup = styled.div`
  background: white;
  color: #3f618c;
  font-weight: 400;
  padding: 5px;
  border-radius: 2px;
`;

const ShowArea = styled.div`
  position: absolute;
  text-align: center;
  top: 0px;
  left: 297px;
  height: 18px;
  width: 150px;
  color: #1a1a1a;
  background-color: white;
  font-weight: 400;
  font-size: 13px;
  padding: 5px;
  border-radius: 2px;
  opacity: .9;
`;

const ShowCoords = styled.div`
  position: absolute;
  text-align: center;
  bottom: 0px;
  left: 50%;
  height: 14px;
  width: 290px;
  color: #1a1a1a;
  background-color: white;
  font-weight: 400;
  font-size: 11px;
  padding: 5px;
  border-radius: 2px;
  opacity: .9;
`;

/////MAPBOX CONFIGS//////
const flyToOptions = {
    speed: 0.8
};

const drawStyle = [
    // ACTIVE (being drawn)
    // line stroke
    {
        "id": "gl-draw-line",
        "type": "line",
        "filter": ["all", ["==", "$type", "LineString"], ["!=", "mode", "static"]],
        "layout": {
            "line-cap": "round",
            "line-join": "round"
        },
        "paint": {
            "line-color": "#009dff",
            "line-dasharray": [0.2, 2],
            "line-width": 2
        }
    },
    // polygon fill
    {
        "id": "gl-draw-polygon-fill",
        "type": "fill",
        "filter": ["all", ["==", "$type", "Polygon"], ["!=", "mode", "static"]],
        "paint": {
            "fill-color": "#00c648",
            "fill-outline-color": "#00c648",
            "fill-opacity": 0.8
        }
    },
    // polygon outline stroke ====> ["get", "vendido", "sim"]
    // This doesn't style the first edge of the polygon, which uses the line stroke styling instead
    {
        "id": "gl-draw-polygon-stroke-active",
        "type": "line",
        "filter": ["all", ["==", "$type", "Polygon"], ["!=", "mode", "static"]],
        "layout": {
            "line-cap": "round",
            "line-join": "round"
        },
        "paint": {
            "line-color": "#D20C0C",
            "line-dasharray": [0.2, 2],
            "line-width": 2
        }
    },
    // vertex point halos
    {
        "id": "gl-draw-polygon-and-line-vertex-halo-active",
        "type": "circle",
        "filter": ["all", ["==", "meta", "vertex"], ["==", "$type", "Point"], ["!=", "mode", "static"]],
        "paint": {
            "circle-radius": 5,
            "circle-color": "#FFF"
        }
    },
    // vertex points
    {
        "id": "gl-draw-polygon-and-line-vertex-active",
        "type": "circle",
        "filter": ["all", ["==", "meta", "vertex"], ["==", "$type", "Point"], ["!=", "mode", "static"]],
        "paint": {
            "circle-radius": 3,
            "circle-color": "#D20C0C",
        }
    },

    // INACTIVE (static, already drawn)
    // line stroke
    {
        "id": "gl-draw-line-static",
        "type": "line",
        "filter": ["all", ["==", "$type", "LineString"], ["==", "mode", "static"]],
        "layout": {
            "line-cap": "round",
            "line-join": "round"
        },
        "paint": {
            "line-color": "#000",
            "line-width": 3
        }
    },
    // polygon fill
    {
        "id": "gl-draw-polygon-fill-static",
        "type": "fill",
        "filter": ["all", ["==", "$type", "Polygon"], ["==", "mode", "static"]],
        "paint": {
            "fill-color": "#009dff",
            "fill-outline-color": "#009dff",
            "fill-opacity": 0.1
        }
    },
    // polygon outline
    {
        "id": "gl-draw-polygon-stroke-static",
        "type": "line",
        "filter": ["all", ["==", "$type", "Polygon"], ["==", "mode", "static"]],
        "layout": {
            "line-cap": "round",
            "line-join": "round"
        },
        "paint": {
            "line-color": "#000",
            "line-width": 3
        }
    }
]

const paintFeatures = {
    "line-width": 3,
    "fill-color": "#ff0000",
    "fill-opacity": 0.5
}

const CADPROJ = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.ak14gdvf/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

const ORTOMOSAIC1 = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.76jdwuqh/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

const ORTOMOSAIC2 = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.09kjb72f/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

const ORTOMOSAIC3 = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.6fjagkp6/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

const ORTOMOSAIC4 = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.5lpjgg9j/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

const ORTOMOSAIC5 = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.b3nlzvnk/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

const ORTOMOSAIC6 = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.38blzxas/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

const ORTOMOSAIC7 = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.2i5j4xug/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

const SOLAR = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.dak897g1/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

const SOLAR2 = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.69k7a8kn/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};


const JFLDTM = {
    "type": "raster",
    "tiles": [
        'https://api.mapbox.com/v4/henriqlima.0s9va79m/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w'
    ],
    "tileSize": 256
};

/*"bounds": [-80.203269,32.966761,80.204055,32.967824],*/

//flyTo: { center: [-118.4107187, 33.9415889], zoom: 11, speed: 0.4 }

// COMPONENT THAT CONTAINS THE MAP AND ALL THE FUNCTIONS
class MetricMap extends Component {

    constructor() {
        super();
        this.state = {
            mapCoords: 'lat: 0, lng: 0',
            selectedProject: undefined,
            features: [],
            fitBounds: undefined,
            center: [-21.811709, -46.586436],
            zoom: [16],
            station: undefined,
            stations: {},
            measure: {
                unit: '',
                type: 'select feature',
                value: 0
            }
        }
    }

    componentDidUpdate() {

        let featuresFromApp = this.props.features

        featuresFromApp.forEach(element => {
            var pageFeature = {
                id: element.id,
                type: element.type,
                geometry: {
                    type: element.geometry.type,
                    coordinates: element.geometry.coordinates,
                },
                properties: { properties: element.properties }
            };
            var addedIds = this.drawControl.draw.add(pageFeature)
            //console.log(addedIds)
        });

        // console.log(typeof(this.props.selectedProject))

        //console.log('Project Features:', this.props.features)
    }
    /////////////////////////////////////

    saveProj = () => {
        let draw = this.drawControl.draw.getAll()
        var featuresArr = []
        var serverArr = []

        //0. CAPTURING ALL FEATURES
        //if (draw.features.length > 0) {
        for (let i = 0; i < draw.features.length; i++) {
            let newFeature = {
                id: draw.features[i].id,
                project: this.props.selectedProject,
                type: draw.features[i].type,
                properties: { Lote: draw.features[i].properties.Lote, Area: draw.features[i].properties.Area },
                geometry: {
                    type: draw.features[i].geometry.type,
                    coordinates: draw.features[i].geometry.coordinates
                },
            }
            serverArr.push(newFeature)
        }

        console.log('1. test', serverArr)

        //1. CHECK IF THE FEATURE IS IN STATE
        if (this.state.features.length > 0) {
            for (var w = serverArr.length - 1; w >= 0; w--) {
                for (var j = 0; j < this.state.features.length; j++) {
                    if (this.state.features[j].id === serverArr[w].id) {
                        serverArr.splice(w, 1);
                        break;
                    }
                }
            }
        }

        console.log('2. newServerArr', serverArr)

        //2. SEND NEW FEATURES ONLY TO THE DB
        if (serverArr.length > 0) {
            axios.post('/metric/features', serverArr)
                .then(result => {
                    console.log(result)
                    //3. PUT NEW FEATURES IN STATE
                    console.log('3. state=>', this.state.features)
                    let copy = Array.from(this.state.features);
                    copy.push(...serverArr)
                    this.setState({
                        features: copy
                    })
                })
                .catch(error => {
                    console.log(error)
                })
        } else {
            console.log('all elements are the same')
        }
    }

    //MAP MOUSE HOVER
    mapHover = (map, evt) => {
        //Actual Coordinates
        //console.log('latlng', evt.lngLat);

        let lng = String((evt.lngLat.lng).toFixed(12))
        let lat = String((evt.lngLat.lat).toFixed(12))
        let myCoords = 'WGS84' + ' | ' + 'lat: ' + lat + ' - lng: ' + lng

        this.setState({ mapCoords: myCoords }) //<--ta dando pau!!
    }

    //MAP FEATURES FUNCTION ACTIVATED ON MAPCLICK
    mapClick = (map, evt) => {

        console.log(this.props.ortoSelector)
        //Actual Coordinates
        //console.log('latlng', evt.lngLat);

        console.log('i need that =>', map.getSource()) //<= IMPORTANT

        console.log('draw controller =>', this.drawControl.draw.options)

        let selectedFeature = this.drawControl.draw.getSelected()

        console.log(selectedFeature)

        if (selectedFeature.features[0]) {
            //Calculating area and setting state
            if (selectedFeature.features[0].geometry.coordinates.length === 1) {
                console.log('é area');
                let polygon = turf.polygon([selectedFeature.features[0].geometry.coordinates[0]])
                var area = turf.area(polygon);
                return this.setState({
                    measure: {
                        unit: 'sq. mts',
                        type: 'area',
                        value: area
                    }
                })
            }
            else if (selectedFeature.features[0].geometry.coordinates.length === 2 && typeof selectedFeature.features[0].geometry.coordinates[0] === 'number') {

                let lat = selectedFeature.features[0].geometry.coordinates[0].toFixed(3)
                let lng = selectedFeature.features[0].geometry.coordinates[1].toFixed(3)

                let coord = lat + ',' + lng

                return this.setState({
                    measure: {
                        unit: 'mts',
                        type: 'coords',
                        value: coord
                    }
                })

            }
            else if (selectedFeature.features[0].geometry.coordinates.length >= 2) {
                console.log('é distancia');
                var line = turf.lineString(selectedFeature.features[0].geometry.coordinates);
                var length = turf.length(line, { units: 'kilometers' });
                return this.setState({
                    measure: {
                        unit: 'mts',
                        type: 'dist',
                        value: length * 1000
                    }
                })
            }
        } else {
            this.setState({
                measure: {
                    unit: '',
                    type: 'no feature',
                    value: 0
                }
            })
        }
    }

    onToggleHover = (quale) => {
        //map.getCanvas().style.cursor = cursor;
        console.log(quale)
    }
    /////////////////////////////////////////

    render() {
        return (
            <div className="App">
                <MuiThemeProvider theme={theme}>
                    <Map
                        style="mapbox://styles/mapbox/satellite-v9"
                        containerStyle={{
                            height: '100vh',
                            width: '100vw'
                        }}
                        center={this.props.location}
                        zoom={this.state.zoom}
                        flyToOptions={flyToOptions}
                        onClick={this.mapClick}
                        onMouseMove={this.mapHover}
                        onDblClick={this.mapRender}
                    >

                        {/* <Layer type="fill" id="thisFill" paint={{
                            "fill-color": "#16161d",
                            "fill-opacity": 0.5
                        }} >
                            {Object.keys(lotes).map((lotesK, index) => (
                                <Feature
                                    key={lotesK}
                                    // onMouseEnter={() => this.onToggleHover('pointer', this)}
                                    // onMouseLeave={() => this.onToggleHover(this)}
                                    onClick={() => this.onToggleHover(this.state.lotes[lotesK].properties)}
                                    coordinates={this.state.lotes[lotesK].geometry.coordinates}
                                />
                            ))}
                        </Layer> */}

                        {/*colocar orto here*/}

                        <Source id="1238" tileJsonSource={ORTOMOSAIC1} />
                        <Layer type="raster" id="1238" sourceId="1238" />

                        <Source id="123" tileJsonSource={eval(this.props.ortoSelector)} />
                        <Layer type="raster" id="123" sourceId="123" />

                        <Source id="456" tileJsonSource={(this.props.cadSelector) ? CADPROJ : undefined} />
                        <Layer type="raster" id="456" sourceId="456" />

                        <Source id="789" tileJsonSource={SOLAR} />
                        <Layer type="raster" id="789" sourceId="789" />

                        <Source id="7892" tileJsonSource={SOLAR2} />
                        <Layer type="raster" id="7892" sourceId="7892" />

                        <DrawControl
                            id='drawer'
                            ref={(drawControl) => { this.drawControl = drawControl; }}
                        />

                        <ZoomControl />
                        <ScaleControl position="bottom-left" />
                        <RotationControl />

                        {/* <Popup style={{ zIndex: 1 }} coordinates={[-46.586611693891, -21.8101]} closeOnClick={true}>
                            <div style={{ color: 'grey' }}>Some annotation:</div>
                            <div style={{ color: 'red' }}>total area: 459,07m2</div>
                        </Popup> */}

                        <Popup style={{ zIndex: 1 }} coordinates={[-46.586514167918, -21.810569009481]} closeOnClick={true}>
                            <Warning style={{ color: 'red' }} />
                        </Popup>

                        <Popup style={{ zIndex: 1 }} coordinates={[-80.203637841447, 32.967442042510]} closeOnClick={true}>
                            <Warning style={{ color: 'red' }} />
                        </Popup>
                    </Map>

                    {(this.state.measure.type === 'area' || this.state.measure.type === 'dist') ?
                        <ShowArea>{this.state.measure.type}: {(this.state.measure.value).toFixed(2)} {this.state.measure.unit}</ShowArea>
                        :
                        <ShowArea>{this.state.measure.type}: {(this.state.measure.value)}</ShowArea>
                    }

                    <ShowCoords>{this.state.mapCoords}</ShowCoords>
                    <Logo></Logo>
                    <Input value="@search..." type="text" />
                    <BottomBar>
                        <Button onClick={this.saveProj} color='intend' variant="raised" size="small">
                            <Save />
                            Save
                        </Button>
                    </BottomBar>
                </MuiThemeProvider>
            </div >
        );
    }
}

export default MetricMap;