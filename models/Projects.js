const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      ObjectId = mongoose.Schema.Types.ObjectId;

// 1. make a Schema
const ProjectsSchema = Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
     location: {
        type: Array
    },
    features: [{
        type: ObjectId,
        ref: 'Features' // this is rquired for populate to work
     }],
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    }
});

// 2. feed that schema into mongoose.model to create the model we interact with
const Projects = mongoose.model('MetricProject', ProjectsSchema)

module.exports = Projects;