const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      ObjectId = mongoose.Schema.Types.ObjectId;


const featuresSchema = Schema({
    id: {
        type: String,
        unique: true,
        required: true
    },
    project: {
        type: Schema.Types.ObjectId,
        ref: 'MetricProject'
    },
    type: {
        type: String,
        default: 'Feature'
    },
    properties: {
        type: Object
    },
    geometry: { 
            type: {type: String}, 
            coordinates: {type: Array}
        }
})

const Features = mongoose.model('MetricFeature', featuresSchema);

module.exports = Features;
