mapboxgl.accessToken = 'pk.eyJ1IjoiZ2VvcmdlLXNpbHZhIiwiYSI6Ii1fcGlpUU0ifQ.5exkucINd7OeX4S2DYGx_w';
var mapView = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
    center: [-48, -19], // starting position [lng, lat]
    zoom: 9 // starting zoom
});

function coerceGeometry (activeLayer, features) {
  var inputCollection = toFeatureCollection(features)
  var geometryType = 'MultiPolygon' // hardcoded
  if (!geometryType) {
    return null
  }
  if (geometryType.startsWith('Multi')) {
    const result = combine(inputCollection)
    return result.features[0].geometry
  } else {
    return features[0].geometry
  }
}


class FeatureEditManager {
  constructor (mapObject, vectLayer) {
    this.map = mapObject
    this.vectLayer = vectLayer
    this.jsonSource = this.map.getSource('parcels-editable')
  }

  _mergeFeatures () {
    const vectFeatures = this.getVectorFeatures()
    const jsonFeatures = this.getJSONFeatures()
    return toFeatureCollection([
      ...vectFeatures,
      ...jsonFeatures
    ])
  }

  getVectorFeatures () {
    return getFeaturesVector(this.map, this.vectLayer)
  }

  getJSONSource () {
    return this.map.getSource(this.jsonLayer.source)
  }

  getJSONFeatures () {
    try {
      return this.jsonSource._data.features
    } catch (err) {
      console.error('erro ao pegar features')
      return []
    }
  }

  updateFilter () {
    var features = this.getJSONFeatures()
    var featFilter = features.reduce((ids, feature) => {
      ids.push(feature.id)
      return ids
    }, ['!in', '$id'])
    console.log('updateFilter')
    this.map.setFilter(this.vectLayer.id, featFilter)
  }

  createJSONFeature (feature) {
    var newFeatures = toFeatureCollection([
      ...this.getJSONFeatures(),
      ...[feature]
    ])
    this.jsonSource.setData(newFeatures)
  }

  updateJSONFeature (feature) {
    var oldFeatures = _.filter(this.getJSONFeatures(), (f) => {
      return f.id !== feature.id
    })
    var newFeatures = toFeatureCollection([
      ...oldFeatures,
      feature
    ])
    this.jsonSource.setData(newFeatures)
    this.updateFilter()
  }

  deleteJSONFeature (feature) {
    var oldFeatures = this.getJSONFeatures()
    var newFeatures = toFeatureCollection([
      ..._.filter(oldFeatures, (f) => {
        return f.id !== feature.id
      })
    ])

    this.jsonSource.setData(newFeatures)
    this.updateFilter()
  }

  onFeatureUpdated (e) {
    console.log('feature update')
    var feature = e.features[0]
    if (!feature) {
      return
    }

    this.updateJSONFeature(feature)
  }
}

function toFeatureCollection (features) {
  if (!features) {
    return null
  }
  if (!_.isArray(features)) {
    features = [features]
  }
  return {
    'type': 'FeatureCollection',
    'features': features
  }
}

function getFeaturesVector (mapObject, vectorLayer) {
  var features = mapObject.queryRenderedFeatures({ layers: [vectorLayer.id] })
  console.log(features)
  if (features) {
    return _.map(_.uniqBy(features, 'id'), (f) => {
      return {...f.toJSON()}
    })
  }
  return null
}
mapView.on('load', () => {
  
  var source = {
    'type': 'vector',
    'tiles': [
      'https://amaps.geoadmin.com.br/propriedades_sigma/{z}/{x}/{y}.pbf'
    ]
  }
  var layer = {
    'id': 'parcels',
    'type': 'fill',
    'source': 'parcels',
    'source-layer': 'propriedades_sigma',
    'paint': {
      'fill-color': '#FF7C00',
      'fill-opacity': 0.3,
      'fill-outline-color': '#FF7C00'
    }
  }
  var cloned = _.cloneDeep(layer)
  var override = {
    'id': 'parcels-editable',
    'paint': {
      'fill-color': '#E900A6',
      'fill-opacity': 0.3,
      'fill-outline-color': '#E900A6'
    },
    'source': {
      'type': 'geojson',
      'data': {
        'type': 'FeatureCollection',
        'features': []
      }
    }
  }
  var editableLayer = {
    ...cloned,
    ...override
  }
  delete editableLayer['source-layer']
  mapView.addSource('parcels', source)
  mapView.addLayer(layer)
  mapView.addLayer(editableLayer)
  var dc = new MapboxDraw()
  mapView.addControl(dc)
  var editMan = new FeatureEditManager(
    mapView,
    layer
  )
  mapView.on('draw.update', _.bind(editMan.onFeatureUpdated, editMan))
  setTimeout(() => {
    
    var features = editMan._mergeFeatures()
    console.log(features)
    dc.add(features)    
  }, 2000)

})