const express = require('express');
const app = express();
const mongoose = require('mongoose')
require('dotenv').config(); // this line reads key-value pairs from .env and puts them in process.env

const DB_URL = process.env.NODE_ENV === 'production' ? process.env.DB_URL : 'mongodb://localhost/MetricDB'
const PORT = process.env.PORT || 8080
////// MAIN CONFIGURATION SCRIPT //////////////////////////////
var bodyParser = require('body-parser');

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE');
    next();
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.use(express.static('./metric-frontend/build'))

// call . connect() to connect to mongod server
mongoose.connect(DB_URL)

const db = mongoose.connection;

db.on('open', () => {
    console.log('connected to mongodb');
});

const Projects = require('./models/Projects');
const Features = require('./models/Features');
/////////////////////////////////////////////////////////////////

/////SERVER ROUTES LOGICS ///////// => PROJECTS
app.get('/metric/projects', (req, res) => {
    //READ
    Projects.find({})
        .then(result => {
            res.send(result);
        })
        .catch(error => {
            console.log(error);
        })
})

app.post('/metric/projects', (req, res) => {
    //CREATE
    console.log(req.body);
    let newProject = Projects(req.body);

    newProject.save()
        .then(savedProject => {
            console.log(savedProject);
            res.json(savedProject);
        })
        .catch(error => {
            console.log(error)
        })
});

//////////////////////////////////////////////


/////SERVER ROUTES LOGICS ///////// => FEATURES
app.get('/metric/features', (req, res) => {
    //READ
    Features.find({})
        .then(result => {
            res.send(result);
        })
        .catch(error => {
            console.log(error);
        })
})

app.post('/metric/features', (req, res) => {
    //CREATE
    //console.log(req.body);

    let newFeature = req.body;

    newFeature.forEach((element) => {
        let newFeature = Features(element);
        newFeature.save()
            .then(savedFeature => {
                console.log(savedFeature);
                res.json(savedFeature)
            })
            .catch(error => {
                console.log(error)
            })
    });
});

app.get('/metric/features/:projectId', (req, res)=>{
    // give me all the features where the project field has the id of the project we're interested in
    Features.find({
        project: req.params.projectId
    })
    .then(features => {
        res.json(features);
    })
    .catch(error => {
    })
})
//////////////////////////////////////////////

app.listen(PORT, () => {
    console.log('server listen at 8080, ctrl+c to exit!')
})